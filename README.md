/* Name: Valeria 
 *  Function: Blink name in Morse Code through green LED
 *  Dots equal to 1 second 
 *  Dashes equal to 3 seconds
 * Space between letters and units is half a second
 * Date: Nov 21, 2018
 */